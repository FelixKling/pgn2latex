# pgn2latex

A service to create print quality pdfs from chess databases in pgn format. A demo is running on http://rcyw289byif8mg8m.myfritz.net:81/ .

This is an old project of mine (from 2017?) and the code quality is poor (no objects, "interesting" design). I updated the code using recursive regex later on, but in general the philosophy while writing the code was just to get it running and preferring simple code introducing some garbage and cleaning up at the end over dealing with every possible scenario explicitly.

## Installation requirements

Python 3 with:
shlex chess unicodedata StringIO flask waitress hashlib
(create a virtual environment named venv in the flask_app folder via "python3 -m venv venv" and
 use pip install <packagenames>)

Latex with:
KOMA
texmate
needspace
xskak
fmtcount
inputenc
multicol
imakeidx
scrlayer-scrpage
hyperref
(installing texlive-full is the simple way, 

    sudo apt install texlive-latex-recommended texlive-latex-extra texlive-games texlive-latex-base

is probably just what you need on Raspbian).

To get it running, add a cronjob that executes "drop_old.sh" every few minutes and runs app.py at reboot:

    @reboot /home/fk/Python/flask_app/venv/bin/python /home/fk/Python/flask_app/app.py
    */15 * * * * /home/fk/Python/flask_app/drop_old.sh

The paths at the beginning of app.py need to be modified according to your username.


## Usage

Copy a chess game in pgn format into the text box and click "process pgn!".

## Special syntax
You can use \sp, \sn, \sb, \sr, \sq and \sk in pgn comments to print piece symbols for pawn, knight, bishop, rook, queen and king. You can also use other Latex commands. However, be careful not to add lose { or } brackets, they will break the output. Also make sure that your chess software accepts comments with { or }.
You can also add Diagrams in ChessBase (i.e. {Diagram #}) or ChessAssistant format, even in side lines. There is sometimes a positioning problem in ChessAssistant format, since the script expects the diagram marker after the move the diagram should be displayed at. For adding a diagram, you can also add DiaW# (without any space behind or in front) to (usually the beginning or end of) a comment.
If there is more than one variation, the script adds an enumeration using a) b) ... and so on. If a variation has several sub-variations, it adds a1) a2) and so on. Do not use too many levels (sub-sub-sub-sub... variations), the script crashes with more than 9 levels (which will also drive your readers crazy).
For positions (not games) just enter Event and White (for instance: Event "rook endings" and White "Lucena position").
The script tries to be flexible, but I cannot test any possible game and it will crash in some cases I just didn't think of.
