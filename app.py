# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see https://www.gnu.org/licenses/


from pgn2latex import pgn2latex
from flask import Flask, request, render_template, jsonify, send_from_directory
from waitress import serve
import hashlib
import zipfile
import os
import subprocess
import sys

# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/home/pi/Python/flask_app/modules')

folders = {
    "temp": "/home/pi/Python/flask_app/temp/",
    "templates": "/home/pi/Python/flask_app/templates/",
    "base": "/home/pi/Python/flask_app/",
}

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/kling/')
def indexk():
    return render_template('index.html')


@app.route('/temp/<path:filename>')
def temp(filename):
    try:
        return send_from_directory(
            folders["temp"],
            filename
        )
    except:
        return render_template('index.html')


@app.route('/pgn2latex/', methods=["GET", "POST"])
def process_pgn():
    content = request.get_json()
    pgn = content["pgn"].strip()
    md5 = hashlib.md5((pgn + str(content)).encode('utf-8',
                      errors='ignore')).hexdigest()
    tex_template = ""

    with open(folders["templates"] + "pgn2latex_template.tex", "r") as file_in:
        for line in file_in:
            tex_template += line

    if pgn == "" or len(pgn) > 2000*500:
        return jsonify(result="Please paste a valid pgn!")
    content["gametext"] = pgn2latex(pgn)

    if len(content["title"]) > 0:
        content["titlesw"] = ""
    else:
        content["titlesw"] = "%"

    if len(content["intro"]) > 0:
        content["introsw"] = ""
    else:
        content["introsw"] = "%"

    if str(content["boardlabelsize"]) == "0":
        content["bboardlabel"] = "false"
    else:
        content["bboardlabel"] = "true"

    if str(content["smallboardlabelsize"]) == "0":
        content["bsmallboardlabel"] = "false"
    else:
        content["bsmallboardlabel"] = "true"

    # set calues in template file
    for c in content:
        tex_template = tex_template.replace(f"##{c}##", content[c])

    tex_path = folders["temp"] + md5 + ".tex"
    with open(tex_path, "w") as tex_file:
        tex_file.write(tex_template)

    command = ["pdflatex", "-interaction=nonstopmode", tex_path,
               "-output-directory", folders["temp"], "-interaction", "nonstopmode"]
    os.chdir(folders["temp"])
    result = subprocess.run(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    os.chdir(folders["base"])

    with zipfile.ZipFile(folders["temp"] + md5 + ".zip", 'w') as zipMe:
        for file in [folders["temp"] + md5 + ".tex", folders["temp"] + md5 + ".pdf"]:
            arcname = file.rsplit("/", 1)[-1]
            zipMe.write(file, arcname=arcname,
                        compress_type=zipfile.ZIP_DEFLATED)

    errors = ["Error:", "Warning:"]
    ignore = ["LaTeX Font Warning: Font shape `LSF/merida/bx/n' undefined",
              "has LaTeX Font Warning", "Some font shapes were not available"]
    info = "\n\n".join([r for r in result.stdout.decode("utf-8", errors='replace').split(
        "\n\n") if any([e in r for e in errors]) and not any([e in r for e in ignore])])
    info = info.strip("\r\n ")
    if info == "":
        info = "Compiled without errors!"

    return jsonify(result=info, pdf="temp/" + md5 + ".pdf", zipf="temp/" + md5 + ".zip")


if __name__ == "__main__":
    #    app.run(debug=True)
    serve(app, host='0.0.0.0', port=80)
