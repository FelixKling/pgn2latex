# -*- coding: utf-8 -*-
#!/usr/bin/env python3

# Tex file for pgn2latex - a free chess game (in pgn format) to Latex converter
# Copyright (C) 2017 Felix Kling
#
# Version 7
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see https://www.gnu.org/licenses/

import shlex
import chess
import chess.pgn
import unicodedata
import regex


def pgn2latex(pgn):

    # defining different lists, dictionaries and conversion functions

    diagramMark = [
        # Chessbase - some stupid guesses :-)
        "Diagramm #",
        "Diagram #",
        "Diagrama #",
        "Диаграма #",
        "Dijagram #",
        "Skeem #",
        "Kaavio #",
        "Διάγραμμα #",
        "Skýringarmynd #",
        "Léaráid  #",
        "Diagramma  #",
        "Диаграм #",
        "Dijagramma #",
        "Схема #",
        # an internal Marker
        "DiaW#",
        "DiaB#",
        "#DiaW",
        "#DiaB"
    ]
    # Diagram from blacks point of view (inverse)?
    diagramMarkBlack = [
        # "DiaB#",
        # "#DiaB"
    ]

    # use these tags only
    tags = [
        "Event",
        "Site",
        "Date",
        "Round",
        "White",
        "Black",
        # "Opening",
        # "ECO",
        "Result"
    ]

    results = {
        "1-0": "\\whiteWins",
        "0-1": "\\blackWins",
        "1/2-1/2": "\\aDraw",
        "*": ""
    }

    results2 = {
        "1-0": "\\rwwins",
        "0-1": "\\rbwins",
        "1/2-1/2": "\\rdraw",
        "*": "\\runclea"
    }
    # we do "not" remove \ or {}, because the user should be allowed to enter latex commands in pgn
    specialLatex = {
        "#": "\\#",
        "&": "\\&",
        "%": "\\%",
        "$": "\\$",
        "_": "\\_",
        "^": "\\^",
    }

    glyphs = [
        "",
        "!",
        "?",
        "!!",
        "??",
        "!?",
        "?!",
        "#"
    ]

    replace = {
        # merge comments
        #    "}{":" ",
        # Aquarium line break
        "^13 ^10": "",
    }

    # Latex line break
    BR = "\n"
    PAR = "\\par "

    numbers = list(range(10))
    numbers = list(map(str, numbers))

    nags = list(range(1, 256))
    nags = list(map(str, nags))
    nags = list("$"+val for val in nags)
    nags = nags[::-1]

    num2alph = {
        " ": "",
        "1": "a",
        "2": "b",
        "3": "c",
        "4": "d",
        "5": "e",
        "6": "f",
        "7": "g",
        "8": "h",
        "9": "i",
    }

    # for handling special characters

    # no chess comment
    miscSymbols = {
        176: "{°}",
        196: "Ä",  # "{\\\"A}",
        214: "Ö",  # "{\\\"O}",
        215: "{\\ensuremath{\\times}}",
        220: "Ü",  # "{\\\"U}",
        223: "ß",  # "{\\ss}",
        228: "ä",  # {\\\"a}",
        246: "ö",  # {\\\"o}",
        252: "ü",  # {\\\"u}",
        710: "{\\^}",
        8201: "{\\,}",
        8208: "{-}",
        8209: "{\\-/}",
        8211: "{--}",
        8212: "{---}",
        8213: "{---}",
        8216: "{`}",
        8217: "{'}",
        # german only
        # 8218:"{\\ensuremath{\\glq}}",
        8220: "{``}",
        8221: "{''}",
        # 8222:"{\\ensuremath{\\glqq}}",
        8491: "{\\AA}",
        8364: "{}",
        8764: "{\\ensuremath{\\sim}}",
        9812: "{\\symking}",
        9813: "{\\symqueen}",
        9814: "{\\symrook}",
        9815: "{\\symbishop}",
        9816: "{\\symknight}",
        9817: "{\\sympawn}",
        9818: "{\\symking}",
        9819: "{\\symqueen}",
        9820: "{\\symrook}",
        9821: "{\\symbishop}",
        9822: "{\\symknight}",
        9823: "{\\sympawn}",
        64257: "",
        64258: "",
    }

    # chess
    chessSymbols = {
        33: "!",
        35: "#",
        43: "+",
        45: "-",
        61: "=",
        63: "?",
        # 78:"N",
        171: "{\\qside}",
        177: "{\\wdecisive}",
        187: "{\\kside}",
        8252: "{!!}",
        8263: "{??}",
        8265: "{!?}",
        8264: "{?!}",
        # zero length non-breaking space
        8288: "",
        8593: "{\\withattack}",
        8594: "{\\withinit}",
        8646: "{\\counterplay}",
        8711: "\\ensuremath{\\nabla}",
        8723: "{\\bdecisive}",
        8734: "{\\unclear}",
        8644: "{\\counterplay}",
        8660: "{\\file}",
        8663: "{\\diagonal}",
        8804: "{\\ensuremath{\\leq}}",
        8805: "{\\ensuremath{\\geq}}",
        8853: "{\\timelimit}",
        8862: "{\\centre}",
        8869: "{\\ending}",
        8722: "{--}",
        8979: "{\\betteris}",
        9632: "{\\onlymove}",
        9633: "{\\onlymove}",
        9651: "{\\withidea}",
        9672: "{\\moreroom}",
        10227: "{\\devadvantage}",
        10752: "{\\zugzwang}",
        10866: "{\\wbetter}",
        10865: "{\\wupperhand}",
        58176: "{\\bishoppair}",
        58177: "{\\opposbishops}",
        58178: "{\\samebishops}",
        58179: "{\\passedpawn}",
        58180: "{\\compensation}",
    }

    # replace x (like in 1. e4 d5 2. exd) with proper capture symbol
    def replace_x(s):
        if s == "":
            return ""
        elif len(s) <= 2:
            return(s)
        out = s[0]
        for charN in range(1, len(s)-1):
            if s[charN] == "x":
                if any(s[charN-1] == str(number) for number in range(1, 10)) and s[charN+1] in [chr(i) for i in range(ord('a'), ord('h')+1)]:
                    out += "{\\takes}"
                    continue
            out += s[charN]
        return out + s[-1]

    combinedSymbols = {
        "\\mbox{+{--}}": "{\\wdecisive}",
        "\\mbox{{--}+}": "{\\bdecisive}",
    }

    def is_number(s):
        try:
            float(s)
            return True
        except ValueError:
            return False
    #
    # the uni2tex function is based on an answer on stackexchange by Khaled Hosny in 2011:
    # https://tex.stackexchange.com/questions/23410/how-to-convert-characters-to-latex-code
    #
    accents = {
        0x0300: '`', 0x0301: "'", 0x0302: '^', 0x0308: '"',
        0x030B: 'H', 0x0303: '~', 0x0327: 'c', 0x0328: 'k',
        0x0304: '=', 0x0331: 'b', 0x0307: '.', 0x0323: 'd',
        0x030A: 'r', 0x0306: 'u', 0x030C: 'v',
    }

    def uni2tex(text, gameNr):
        out = ""
        txt = tuple(text)
        # search non-breaking empty spaces and replace them with mboxes
        while text.find(chr(8288)) != -1:
            pos = text.find(chr(8288))
            text = text[:pos-1] + \
                "\\mbox{"+text[pos-1]+text[pos+1]+"}"+text[pos+2:]
        for i in range(len(text)):
            char = text[i]
            code = ord(char)
            if unicodedata.category(char) in ("Mn", "Mc") and code in accents:
                out += "\\%s{%s}" % (accents[code], txt[i+1])
                i += 1
            # precomposed characters
            elif len(unicodedata.decomposition(char).split()) == 2 and is_number(unicodedata.decomposition(char).split()[0]):
                base, acc = unicodedata.decomposition(char).split()
                acc = int(acc, 16)
                base = int(base, 16)
                if acc in accents and False:  # Das braucht man nicht
                    out += "\\%s{%s}" % (accents[acc], chr(base))
                else:
                    out += char
            elif code > 127:
                if code in chessSymbols:
                    out += chessSymbols[code]
                elif code in miscSymbols:
                    out += miscSymbols[code]
                # if an improper (not combine) macron is used after a number
                elif code == 175:
                    before = text[i-1]
                    out = out[:i-2]
                    out += "\\={" + before + "}"
                else:
                    out += char
                    # if i > 20:
                    #print("unrecognized symbol in game",gameNr, ":", char, "position:", i+1, "char code:", code,  "\"",text[i-20 : i+1], "\"")
                    # else:
                    #print("unrecognized symbol in game",gameNr, ":", char, "position:", i+1, "char code:", code,  "\"",text[ : i+1], "\"")
            else:
                out += char
            # combining marks
        for sym in combinedSymbols:
            out = out.replace(sym, combinedSymbols[sym])
        return out

    def processGame(content, fenboard):

        if content == "" or content in results.keys():
            #print("empty game")
            return "\\end{samepage}"+"\n\n"

        # special latex stuff: replace curly brackets
        content = regex.sub(r"({([^{}]|(?R))*})", r"###\1###", content)
        content = content.replace("###{", "<!<").replace("}###", ">!>")
        content = content.replace("{", "<#<").replace("}", ">#>")
        content = content.replace("<!<", "{").replace(">!>", "}")

        # Aquarium diagram markers to Chessbase markers for simplicity
        diagramAqua = ["[%t Dgrm] DA", "[%t bDgr] DB"]
        for aquaMarker in diagramAqua:
            content = content.replace(aquaMarker, "Diagramm #")
        # comment markers in Aquarium
        commentAqua = ["[%t Shrt] ", "[%t Long]"]
        for aquaMarker in commentAqua:
            content = content.replace(aquaMarker, "")

        # remove other special comments from Aquarium
        content = regex.sub(r"{\[%[^\]]+\].*?}", r"", content)

        # remove empty comments
        content = content.replace("{}", "%%%%%%%%%")
        content = content.replace("{ }", "%%%%%%%%%")

        # moving NAGs
        # these NAGs (140-145) occur only at the beginning of variations and need to be in front of the move.
        # 1. e4 e5 $140 2. Nf6 $140 * -> 1. e4 %%%140%%% e5 %%%140%%% 2. Nf6 *
        content = regex.sub(
            r"((?:\d+\. )?\w+) \$(14[0-5])", r"%%%\2%%% \1", content)

        # look for short comments after a long comment and swap them
        content = content.replace("}", "} ")
        # remove double spaces
        content = regex.sub(r"[ ]+", r" ", content)

        conSplit = regex.split(r'(\{(?:[^\{]+|(?R))\})', content)
        newContent = ""
        for i in range(len(conSplit)):
            if i < (len(conSplit)-2):
                if conSplit[i].startswith("{") and conSplit[i+1] == " " and conSplit[i+2].startswith("{")\
                        and regex.match('[^\w]*[\w]+.*', conSplit[i+2]) == None:  # short comments don't contain characters
                    temp = conSplit[i]
                    conSplit[i] = conSplit[i+2]
                    conSplit[i+2] = temp
            newContent += conSplit[i]
        content = newContent
        content = content.replace("} {", " ")

        # add missing move numbers for the first black move after a side line or comment
        # 1. e4 (1. d4) e5
        content = regex.sub(
            r"(\d+\.)( [\w-+x!?]+ (?:\$\d+ ?)*({([^{}]|(?3))*} ?)*(\(([^()]|(?5))*\))* ?({([^{}]|(?7))*} ?)?(?:%%%[^%]*%%% ?)*)([\w-+x!?]+)", r"\1\2\1.. \9", content)

        # remove double spaces
        content = regex.sub(r"[ ]+", r" ", content)

        # remove wrong N... moves, ignore the first move of the game
        # 1.e4 1... e5
        content = regex.sub(
            r"(\d+)(\. [\w-+x!?]+(?: %%%\d+%%%)* ?)(\1\.\.\. )", r"\1\2", content)

        # detemine structure
        contentNoSpace = content.replace(" ", "")
        level = 0
        structure = []
        comment = False
        for i in range(len(contentNoSpace)):
            if contentNoSpace[i] == "{":
                comment = True
            elif contentNoSpace[i] == "}":
                comment = False
            elif contentNoSpace[i] == "(" and comment == False:
                level += 1
                structure.append([level, 0])
            elif contentNoSpace[i] == ")" and comment == False:
                level -= 1
                # check if another line follows directly
                if contentNoSpace[i+1] != "(":
                    structure.append([level, 1])
                else:
                    structure.append([-1, 1])

        # count how many entries per level
        levels = [0] * 10

        # how many entries per level occurence
        levelsN = [[] for i in range(10)]
        for i in range(len(structure)):
            if structure[i][0] != 0 and structure[i][0] != -1:
                level = structure[i][0]
                if level > 9:
                    #print("too many variation levels, stopping!")
                    raise SystemExit
                if structure[i][1] == 0:
                    levels[level] += 1
                if structure[i+1][0] == (level - 1):
                    levelsN[level].append(levels[level])
                    levels[level] = 0
    #    #print(levelsN)
    #    #print("structure:",structure)
        symbols = [[] for i in range(10)]
        for i in range(10):
            if len(levelsN[i]) != 0:
                for j in range(len(levelsN[i])):
                    if levelsN[i][j] > 1:
                        for k in range(levelsN[i][j]):
                            symbols[i].append(str(k+1))
                    else:
                        symbols[i].append("")
    #    #print(symbols)
        brackets = []
        closedBracketsN = 0
        level = 0
        levels = [0] * 10
        symbol = ""
        comment = False
        printDiagram = ""
        # is it a enumeration, for ending lines of level>1 enumerations
        isenum = 0
        for i in range(len(content)):
            if content[i] == "{":
                comment = True
            if content[i] == "}":
                comment = False
            if content[i] == "(" and comment == False:
                level += 1
                symbol += symbols[level][levels[level]]
                if symbols[level][levels[level]] == "" and level > 1:
                    symbol += " "
    #            #print(symbol)
                levels[level] += 1
                if len(symbol) > 1:
                    if symbol.find(" ") != -1:
                        brackets.append("")
                    else:
                        brackets.append(num2alph[symbol[0]]+symbol[1:])
                elif len(symbol) == 1:
                    brackets.append(num2alph[symbol[0]])
                else:
                    brackets.append(symbol)
            if content[i] == ")" and comment == False:
                level -= 1
                symbol = symbol[:-1]
                closedBracketsN += 1

        # print(symbols)
    #    if fenboard != "":
        output = "\\mainline[level=1]{"

        level = 0
        # how many brackets open
        bracket = 0
        closedBrackets = 0
        # open brackets
        bracketOpen = []
        # inside a comment?
        comment = False
        text = ""
        # separate introduction for variation
        pretext = ""
        # store mainline diagrams
        maindia = ""
        # check if quotation marks in comment
        quoteStart = True

        iterall = iter(range(len(content)))
        for i in iterall:
            if content[i] == "{" and comment == False:
                comment = True
                if level == 0:
                    pretext += "}\n "
                else:
                    pretext += "\\xskakcomment{ "
            elif content[i] == "}" and comment == True:
                comment = False
                #replace # with \#

                for marker in diagramMark:
                    if text.find(marker) != -1:
                        # Diagram from blacks point of view (inverse)?
                        inverseDiagram = False
                        if marker in diagramMarkBlack:
                            inverseDiagram = True
                        printnow = False
                        if text.find(marker+"!") != -1:
                            printnow = True
                        # remove marker
                        text = text.replace(marker+"!", "<<<")
                        text = text.replace(marker, "<<<")
                        # main line is simple
                        if level == 0:
                            if inverseDiagram:
                                maindia = "\n{\\par \\centering\\chessboardn[inverse] \\par}\n"
                            else:
                                maindia = "\n{\\par \\centering\\chessboardn \\par}\n"

                            if printnow:
                                pretext = "\\dia} " + \
                                    text[:text.find("<<<")] + \
                                    maindia + "{"+pretext
                                text = text[text.find("<<<")+3:]
                                maindia = ""
                            else:
                                pretext = "\\dia}{"+pretext
                        # now the fun starts - a diagram inside a variation :-/
                        else:
                            # parse the moves before
                            getmoves = content[:i + 1]
                            # strip comments
                            getmoves = regex.sub(
                                r"({([^{}]|(?R))*})", r"", getmoves)
                            # strip variations
                            getmoves = regex.sub(
                                r"(\(([^()]|(?R))*\))", r"", getmoves)
                            # make sure a ( has a space in front
                            getmoves = getmoves.replace("(", " (")
                            # remove double spaces
                            getmoves = regex.sub(r"[ ]+", r" ", getmoves)
                            # remove moves in front of a bracket (line start)
                            # 1. e4 e5 (1... c5) 2. c3 (2. d3)(2. Nf3)
                            getmoves = regex.sub(
                                r"(\d+[.]([.]{2})? )?\w+ \((\d+\.\.\. )?", r"", getmoves)
                            # remove black moves starting with number and ...
                            getmoves = regex.sub(r"\d+\.\.\. ", r"", getmoves)

                            # remove NAGs
                            getmoves = regex.sub(r"\$\d+ ", r"", getmoves)
                            # and our special NAGs
                            getmoves = regex.sub(r"%%%\d+%%% ", r"", getmoves)

                            # trailing spaces
                            getmoves = getmoves.strip()

                            # moves browsed!
                            # create a pgn
                            if fenboard != "":
                                getmoves = "[SetUp \"1\"]\n[FEN \"" + \
                                    fenboard+"\"]\n\n" + getmoves
                            else:
                                getmoves = "\n"+getmoves
                            getmoves = "[Event \"?\"]\n[Site \"?\"]\n[Date \"????.??.??\"]\
                            \n[Round \"?\"]\n[White \"Test\"]\n[Black \"Test\"]\
                            \n[Opening \"?\"]\n[ECO \"?\"]\n[Result \"1-0\"]\n"+getmoves
                            # get FEN
                            from io import StringIO
                            pgn = StringIO(getmoves)
                            game = chess.pgn.read_game(pgn)
                            fen = game.end().board().fen()

                            if inverseDiagram:
                                printDiagram += "\n{\\par \\centering\\chessboardt[inverse,setfen="+fen+"] \\par}\n"
                            else:
                                printDiagram += "\n{\\par \\centering\\chessboardt[setfen="+fen+"] \\par}\n"
                            pos = pretext.find("\\xskakcomment{")
                            if printnow:
                                pretext = pretext[:pos+14] + \
                                    "\\diav{}" + pretext[pos+15:]
                                spaceChar = "~"
                                if text[0] not in [")", "]"]:
                                    spaceChar = "{} "
                                pretext += spaceChar + \
                                    text[:text.find(
                                        "<<<")] + "}}" + printDiagram + "\\variation[level="+str(level+1)+"]{\\xskakcomment{"
                                text = text[text.find("<<<")+3:]
                                printDiagram = ""
                            else:
                                pretext = pretext[:pos+14] + \
                                    "\\diav{}" + pretext[pos+14:]

                # ignore colour markers from Aquarium!
                # if not empty now, add comment
                if text.replace(" ", "") != "" or pretext.find("\\dia") != -1:
                    if level == 0:
                        # check if special chess symbols after the move,
                        # which should be placed directly after it
                        shortComment = False
                        stopShort = False
                        newText = ""
                        for char in text:
                            if ord(char) not in chessSymbols:
                                stopShort = True

                            if stopShort:
                                newText += char
                            else:
                                if shortComment == False:
                                    shortComment = True
                                    output += " \\xskakcomment{"
                                output += char
                        if shortComment:
                            output += "}"
                        text = newText
                        # check if comment is empty
                        if text.replace(" ", "") != "" or pretext.find("\\dia") != -1:
                            text = replace_x(text)
                            if content[i+1:i+3].find("(") == -1 and content[i+1:i+3].find("{") == -1:
                                text += "\n\n"+maindia+"\\mainline[level=1]{"
                                maindia = ""
                            else:
                                text += "\n\n\\mainline[level=1]{"
                        else:
                            text = ""
                            pretext = ""
                    else:
                        # put chess symbols directly after move
                        shortComment = False
                        stopShort = False
                        newText = ""
                        newMoveText = ""
                        for char in text:
                            if ord(char) not in chessSymbols:
                                stopShort = True

                            if stopShort:
                                newText += char
                            else:
                                if shortComment == False:
                                    shortComment = True
                                newMoveText += char
                        pos = pretext.find("\\xskakcomment{")
                        pretext = pretext[:pos+14] + \
                            newMoveText + pretext[pos+14:]
                        text = newText
                        # remove space if empty comment
                        if text.replace(" ", "") == "":
                            if pretext[-1] == " ":
                                pretext = pretext[:-1]
                                # mark the short comment
                                pretext += "}***"
                        else:
                            text = replace_x(text)
                            text += "}} \\variation[level="+str(level+1)+"]{"
                    for key in specialLatex.keys():
                        text = text.replace(key, specialLatex[key])
                    output += pretext+text

                text = ""
                pretext = ""
            elif content[i] == "(" and comment == False:
                bracketOpen.append(1)
                level += 1
                output += "}"
                output += printDiagram
                printDiagram = ""
                # no line finished or started before
                if content[i-2:i].find(")") == -1 and content[i-2:i].find("(") == -1:
                    # no enumeration
                    if brackets[bracket] == "":
                        # new line if comment before
                        if content[i-1] == "}" or content[i-2] == "}":
                            if level == 1:
                                output += BR
                        if level == 2:
                            output += "\\\\("
                        if level > 2:
                            output += " ("
    #                    else:
    #                        output += "\\noindent{}"

                if level == 1:
                    output += BR+BR
                bufout = "\\variation[level="+str(level+1)+"]{"

                # if enumeration
                if brackets[bracket] != "":
                    spaces = ""
                    if content[i-1] == "}" or content[i-2] == "}":
                        # if first entry of enumeration after comment in the main line
                        if level == 1 and brackets[bracket][0] == "a":
                            bufout = BR+BR+bufout
                    if brackets[bracket][-1] != "a":
                        if level == 1:
                            bufout = BR+BR+bufout
                    if level > 1:
                        # new line and indent
                        if len(brackets[bracket]) > 1:
                            bufout = "\n" + \
                                (len(brackets[bracket])-1)*"\t" + \
                                "{\\addtolength{\\leftskip}{1mm}"+bufout
                        else:
                            bufout = "\n{"+bufout
                        # store that is enum to finish line with line break
                        isenum += 1
                        # first line of a level>1 enumeration
                        if brackets[bracket][-1] == "a" or brackets[bracket][-1] == "1":
                            bufout = "\\par"+bufout
                    bufout += "\\xskakcomment{\\textbf{" + \
                        brackets[bracket]+")} } "

                output += bufout

                bracket += 1

            elif content[i] == ")" and comment == False:
                closedBra = 0
                closedBrackets += 1
                for bra in range(len(bracketOpen)):
                    if bracketOpen[-1-bra] == 1:
                        bracketOpen[-1-bra] = 0
                        closedBra = len(bracketOpen)-1-bra
                        break
                level -= 1
                output += "}"

                # no enumeration?
                if brackets[closedBra] == "":
                    if level == 1:
                        output += ")\\\\"
                    if level > 1:
                        output += ") "
                    # check if several brackets close:
                    if content[i+1:i+3].find(")") == -1:
                        output += printDiagram
                        printDiagram = ""
                else:
                    # check if several brackets close:
                    if content[i+1:i+3].find(")") == -1:
                        output += printDiagram
                        printDiagram = ""

                    # end line for enumeration
                    if isenum > 0:
                        output += "\\par}"
                        isenum -= 1
                    if level > 0:
                        output += " "
                if level == 0:
                    if content[i+1:i+3].find("(") == -1 and content[i+1:i+3].find("{") == -1:
                        output += "\n\n"+maindia+"\\mainline[level=1]{"
                        maindia = ""
                    else:
                        output += "\n\n\\mainline[level=1]{"
                else:
                    output += "\n\\variation[level="+str(level+1)+"]{"

            elif comment == True:
                # check if quotation marks
                if content[i] == "\"":
                    text += "\\qqstart{}" if quoteStart else "\\qqend{}"
                    quoteStart = not quoteStart
                else:
                    text += content[i]
            elif content[i] == "%":
                # we marked the NAGs that are in front of moves and replace them now:
                if content[i:i+3] == "%%%":
                    if content[i:i+9] == "%%%140%%%":
                        output += "\\xskakcomment{\\withidea} "
                    if content[i:i+9] == "%%%141%%%":
                        output += "\\xskakcomment{$\\nabla$} "
                    if content[i:i+9] == "%%%142%%%":
                        output += "\\xskakcomment{\\betteris} "
                    elif content[i:i+9] == "%%%143%%%":
                        output += "\\xskakcomment{\\worseis} "
                    elif content[i:i+9] == "%%%144%%%":
                        output += "\\xskakcomment{\\equalis} "
                    elif content[i:i+9] == "%%%145%%%":
                        output += "\\xskakcomment{\\chesscomment} "
                    elif content[i:i+9] == "%%%%%%%%%":
                        output += "{}"
                    for dummy in range(8):
                        next(iterall)
                else:
                    output += content[i]
            elif content[i] == "-" or content[i] == "Z":
                # ignore non-nullmove minus
                if (content[i:i+2] == "--" or content[i:i+2] == "Z0"):
                    # ignore main line null moves and null moves in front of comments
                    if level > 0 and content[i+3:i+5].find("{") == -1:
                        # white move in front
                        if content[i-2] == "." and content[i-3] != ".":
                            output += "\\xskakcomment{. -- }"
                            # find next move and add it to comment, this a text with only one space
                            spacesMax = 2
                            spaces = 0
                            start = i+1
                            aVar = 1
                            while spaces < spacesMax:
                                if content[start+aVar] == " ":
                                    spaces += 1
                                    if content[start+aVar+1] == "$":
                                        spacesMax += 1
                                if content[start+aVar] == ")":
                                    break
                                if spaces != 1 or content[start+aVar] != " ":
                                    output += content[start+aVar]
                                next(iterall)
                                aVar += 1
                            output += "} \\variation[level="+str(level+1)+"]{"
                        else:
                            output += "}\\variation[level=" + \
                                str(level+1)+"]{\\xskakcomment{ -- }"
                        next(iterall)
                    # mainline nullmove, just remove the complete move for game continuation in Aquarium
                    elif level == 0:
                        # white move in front, remove number, too
                        if content[i-2] == "." and content[i-3] != ".":
                            # find move number
                            avar = 3
                            while is_number(output[-avar]):
                                avar += 1
                            # delete it
                            output = output[:(-avar+1)]
                        # black move in front, remove number, too, if variation starts before
                        elif content[i-4:i-1] == "...":
                            # find move number and check if variation just starts
                            avar = 5
                            while (is_number(output[-avar]) or output[-avar] == " "):
                                avar += 1
                            # does a variation start?
                            if output[-avar] == "{":
                                # delete move number
                                output = output[:(-avar+1)]
                        # just ignore black move
                        next(iterall)
                    else:
                        output += content[i]
                else:
                    output += content[i]
            else:
                output += content[i]
        if output[-2:] == "* ":
            output = output[:-2]+"} "+results2["*"]
        elif output[-4:] == "1-0 ":
            output = output[:-4]+"} "+results2["1-0"]
        elif output[-4:] == "0-1 ":
            output = output[:-4]+"} "+results2["0-1"]
        elif output[-8:] == "1/2-1/2 ":
            output = output[:-8]+"} "+results2["1/2-1/2"]

        # clean up output - some "dirty" tricks to handle rare cases neglected above and simplifying the code
        output = output.replace("<<<", "")
        output = regex.sub(r"[ ]+", r" ", output)
        output = regex.sub(
            r"(?:\n)?\\(?:first)?mainline\[level=1\]{[ ]?}", r" ", output)
        output = regex.sub(
            r"(?:\n)?\\(?:first)?variation\[level=[1-9]\]{[ ]?}", r" ", output)
        output = output.replace(" \\dia", "\\dia")
        output = output.replace("\n{}\n", "\n")
        # many empty lines
        output = output.replace("\n \n", "\n")
        # linebreak and empty line
        output = output.replace("\\\\\n\n", "\n\n")
        output = regex.sub(r"\n{3,}", r"\n\n", output)

        # bracket closing variation in empty line
        output = output.replace("\\\\ )", ")")
        output = output.replace("\\\\)", ")")
        # redundant \pars
        for i in range(10):
            j = 9-i
            output = output.replace("\\par}"+j*" \\par}", "\\par}"+j*" }")
        # line break after \par
        output = output.replace("\\par}\n\\\\", "\\par}\n")
        # remove empty game body
        output = output.replace("\\mainline[level=1]{ 1. }", "")
        # no space after diagram
        output = output.replace("} )", "})")
        output = output.replace("1/2-1/2", "\\aDraw")
        output = output.replace("1-0", "\\whiteWins")
        output = output.replace("0-1", "\\blackWins")
        # comment at the begin of a variation: remove leading space
        output = output.replace("]{\\xskakcomment{ ", "]{\\xskakcomment{")
        # but not for null move
        output = output.replace("]{\\xskakcomment{--", "]{\\xskakcomment{ --")
        # remove space after null move followed by new variation
        output = output.replace(
            "-- } } (\\variation[level=", "--} } (\\variation[level=")
        # remove empty comments:
        output = output.replace("\\xskakcomment{ }", "")
        output = output.replace("\\xskakcomment{}", "")
        # comments starting with a colon
        output = output.replace("\\xskakcomment{ ,", "\\xskakcomment{,")
        # double space after enumeration if comment starts with space:
        output = output.replace(" } \\xskakcomment{ ", " } \\xskakcomment{")
        # check variations starting with ...
        index = 1
        while index < len(output):
            index = output.find("...", index)
            if index == -1:
                break
            index2 = index-1
            # ok, now find position when move number ends
            while (output[index2] != " "):
                index2 -= 1
            if output[index2+1:index].isdigit():
                if output[index2-1] != "{" and output[index2-1] != "}" and output[index2-2] != "}" and output[index2-2] != "{":
                    output = output[:index2+1]+output[index+3:]
                elif output[index2-4:index2-1] == "***":
                    output = output[:index2+1]+output[index+3:]
                # check if xskakcomment in front
                elif output[index2-1] == "}" or output[index2-2] == "}":
                    indexN0 = index2-1
                    while output[indexN0] != "{":
                        indexN0 -= 1
                    indexN1 = indexN0-1
                    while output[indexN1] != "\\":
                        indexN1 -= 1
                    if output[indexN1+1:indexN0] == "xskakcomment":
                        if output[indexN1-1] != "{" and output[indexN1-2] != "{" or output[indexN1-4:indexN1-1] == "***":
                            output = output[:index2+1]+output[index+3:]

            index += 3
        # remove short comment markers
        output = output.replace("***", "")
        # removing special chars
        output = uni2tex(output, gameNo)

        # empty moves in mainline like 4... 1-0
        output = regex.sub(
            r"\\mainline\[level=\d\]{ \d+[.]{1,3} }[ ]?", r"", output)

        # make sure the result is on the same page as the last main line
        if True:  # not noResult:
            position = output.rfind("\\mainline")
            if output.rfind("\\variation") > position:
                position = output.rfind("\\variation")
            if output.rfind("\\noindent") > position:
                position = output.rfind("\\noindent")
            output = output[:position]+"\\begin{samepage}"+output[position:]
            output += "\\end{samepage}"

        output = regex.sub(r" {2,}", r" ", output)

        # add curly latex brackets again
        output = output.replace("<\\#<", "{").replace(">\\#>", "}")

        # remove X... after short comments
        output = regex.sub(
            r"(\d+\.)( [a-h1-8NBRQKOx+!?#-.p]+ \\xskakcomment{[^}]*} )\1[.]{2} ", r"\1\2", output)

        #print("Game processed.")
        return output+"\n\n"

    ############## MAIN PART ##############
    # store output to return it later on
    result_return = ""

    # remove \r from windows line breaks
    read = pgn.replace("\r", "").split("\n")

    gameNo = 0

    #where in game
    head = False
    body = False

    # the game body as string
    content = ""
    # board at the beginning?
    fenboard = ""

    for line in read:
        # remove leading and trailing spaces
        line = line.strip()
        if line == "":
            # end of head?
            if head:
                head = False
                body = True
                result_return += ("\\gameHeader\n\n")
                # do we start with a position other than the start position?
                if fenboard != "":
                    result_return += (
                        "\n{\\par \\centering\\chessboardn[addfen="+fenboard+"] \\par}\n")
            continue
        elif (line[0] != '[' or line[-1:] != ']' or line[-2] != '"'):
            content += line + " "
            for entry in replace:
                content = content.replace(entry, replace[entry])

        # process tags
        else:
            # head starts?
            if head == False:
                body = False
                # don't process after first header
                if gameNo > 0:
                    # process game
                    #print("Processing game",gameNo)
                    result_return += processGame(content, fenboard)
                    content = ""
                    fenboard = ""
                result_return += ("\\newgame\n"+"\\resetPgnTags\n")
                gameNo += 1
            # ok, we found the head
            head = 1
            # remove first and last char
            line = line[1:-1]

            # get tag type and content (in quotation marks)
            split = shlex.split(line)

            # standard tags
            if split[0] in tags:
                # special fields
                if split[0] == "Result":
                    split[1] = results[split[1]]
                if split[0] == "Date":
                    split[1] = split[1].replace(".??", "")
                    split[1] = split[1].replace("?", "")
                    split[1] = split[1].split(".")[0]
                if split[0] == "White":
                    split[0] = "WhitePlayer"
                    split[1] = split[1].replace("?", "")
                if split[0] == "Black":
                    split[0] = "BlackPlayer"
                    split[1] = split[1].replace("?", "")
                if split[1].replace("?", "") == "":
                    split[1] = ""
                # remove special chars
                for key in specialLatex.keys():
                    split[1] = split[1].replace(key, specialLatex[key])
                split[1] = uni2tex(split[1], gameNo)
                # write formatted tag
                result_return += ("\\def\\"+split[0]+"{"+split[1]+"}\n")

            # starting position
            elif split[0] == "FEN":
                result_return += ("\\fenboard"+"{"+split[1]+"}\n")
                fenboard = split[1]

    # last game
    #print("Processing game",gameNo)
    result_return += processGame(content, fenboard)

    return result_return

#    #print("All games processed!",file_out,"written.")
#    print ("Done, closing.")
    #raise SystemExit
